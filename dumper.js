var fs = require('fs');
var request = require('request');
var async = require('async');
var _ = require('lodash');
var sprintf = require('sprintf-js').sprintf;

var API = require('./API');

var token = '913b4ce2249fedbedf9d1b9a281a35bb4d449fe7289c24111a1ed39067fc937ada6a83cf1dfe80449be96';
var api = new API(token);

var dialogId = 157;

var offset = 0;
var totalCount = 0;
var messagesCount = 0;

var rawData = [];

var startTime = new Date().getTime();

async.doWhilst(function(callback) {
	console.log('Requesting messages from dialog %d, offset = %d...', dialogId, offset);

	api.getMessages(offset, dialogId, function(err, data) {
		if(err) {
			callback(err);
		} else {
			totalCount = data.totalCount;
			offset = data.nextOffset;

			var messages = data.messages;

			rawData = _.concat(rawData, messages);
			messagesCount = rawData.length;

			var needCount = totalCount - offset;

			console.log('Received %d messages from dialog %d, need %d messages, total count = %d\n',
				messages.length, dialogId, needCount < 0 ? 0 : needCount, messagesCount);

			callback();
		}
	});
}, function() {
	return offset < totalCount;
}, function(err) {
	var messagesDownloadTime = new Date().getTime() - startTime;

	console.log('All messages received in %d millis\n', messagesDownloadTime);

	var users = _.chain(rawData)
		.map(function(message) {
			return message.user;
		})
		.uniq()
		.value();

	console.log('Current dialog includes %d users', users.length);
	console.log('Resolving users information...');

	async.waterfall([
		function(callback) {
			api.getUsersInfo(users, callback);
		},
		function(usersExtendedInfo, callback) {
			console.log('Users information received!\n');
			console.log('Generating output file...');

			var stream = fs.createWriteStream('messages.txt', {
				flags: 'w'
			});

			stream.once('open', function() {
				callback(null, stream, usersExtendedInfo);
			});
		}
	], function(err, stream, usersExtendedInfo) {
		console.log('Output file stream opened!');

		_.forEach(rawData, function(message) {
			stream.write(sprintf('%s:\n', usersExtendedInfo[message.user]));
			stream.write(sprintf('%s\n\n', message.message));
		});

		stream.end();

		console.log('Output file stream closed!\n');
		console.log('We are worked for %d ms and now finished. Have a nice day :)', new Date().getTime() - startTime);
	});
});