var request = require('request');
var async = require('async');
var sprintf = require('sprintf-js').sprintf;
var _ = require('lodash');

function API(token) {
	this.token = token;

	this.getMessages = function getMessages(offset, dialogId, callback) {
		var baseUrl = sprintf('https://api.vk.com/method/execute.getMessages?%%s&v=5.53&access_token=%s', token);

		async.waterfall([
			function(callback) {
				var params = [ sprintf('offset=%d', offset), sprintf('dialogId=%d', dialogId) ].join('&');
				var url = sprintf(baseUrl, params);

				request(url, function(error, response, body) {
					callback(error, body);
				});
			},
			function(raw, callback) {
				var response = JSON.parse(raw).response;

				var data = _.chain(response.messages)
					.flatten()
					.map(function(message) {
						var text = message.body;
						
						if(text.length > 0) {
							return {
								user: message.from_id,
								message: text
							};
						}
					})
					.compact()
					.value();

				callback(null, {
					totalCount: response.totalCount,
					nextOffset: response.nextOffset,
					messages: data
				});
			}
		], callback);
	}

	this.getUsersInfo = function getUsersInfo(users, callback) {
		var baseUrl = sprintf('https://api.vk.com/method/users.get?%%s&v=5.53&access_token=%s', token);

		async.waterfall([
			function(callback) {
				var params = sprintf('user_ids=%s', users.join(','));
				var url = sprintf(baseUrl, params);

				request(url, function(error, response, body) {
					callback(error, body);
				});
			},
			function(raw, callback) {
				var usersExtendedInfoRaw = JSON.parse(raw).response;
				var usersExtendedInfo = {};

				_.forEach(usersExtendedInfoRaw, function(user) {
					usersExtendedInfo[user.id] = [ user.first_name, user.last_name ].join(' ');
				})

				callback(null, usersExtendedInfo);
			}
		], callback);
	}
}

module.exports = exports = API